package com.pocketmath.exception;

/**
 * Created by vishnuhr on 18/3/15.
 * Exception thrown while doing admin tasks: create / delete table
 */
public class HbaseAdminException extends Exception{

    public HbaseAdminException(String message, Throwable cause) {
        super(message, cause);
    }

    public HbaseAdminException(String message) {
        super(message);
    }
}
