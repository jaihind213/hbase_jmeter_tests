package com.pocketmath.util;

import org.apache.hadoop.hbase.util.Bytes;

/**
 * @author Pedro Gandola <pedro.gandola@pocketmath.com>.
 */
public enum BidDetailsColumnFamily {
    DETAILS,
    COUNTERS;

    private byte[] nameBytes;

    private BidDetailsColumnFamily() {
        nameBytes = Bytes.toBytes(this.name());
    }

    public byte[] getNameBytes() {
        return nameBytes;
    }
}
