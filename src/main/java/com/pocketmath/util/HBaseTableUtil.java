package com.pocketmath.util;

import com.pocketmath.exception.HbaseAdminException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.io.hfile.Compression;

import java.io.IOException;
import java.util.List;

/**
 * Created by vishnuhr on 18/3/15.
 */
public final class HBaseTableUtil {

    private HBaseTableUtil() {
    }

    public synchronized static void createTable(final String tableName, final List<String> columnFamilies,
                                                final Configuration configuration, String compressionAlgorithm) throws HbaseAdminException {

        if (tableName == null || tableName.isEmpty()) {
            throw new IllegalArgumentException("Cant create table with empty table name!");
        }

        if (columnFamilies == null || columnFamilies.size() == 0) {
            throw new IllegalArgumentException("Cant create table with 0 column families!");
        }

        if(compressionAlgorithm == null || compressionAlgorithm.isEmpty()){
            compressionAlgorithm= Compression.Algorithm.NONE.getName();
        }

        try (final HBaseAdmin admin = new HBaseAdmin(configuration)) {
            if (!admin.tableExists(tableName)) {
                final HTableDescriptor tableDescriptor = new HTableDescriptor(tableName);
                // create all column families.
                final Compression.Algorithm compressAlgoEnum = Compression.Algorithm.valueOf(compressionAlgorithm);
                columnFamilies.forEach(columnFamily -> {
                    final HColumnDescriptor hColumnDescriptor = new HColumnDescriptor(columnFamily);
                    hColumnDescriptor.setCompressionType(compressAlgoEnum);
                    tableDescriptor.addFamily(hColumnDescriptor);
                });
                admin.createTable(tableDescriptor);
            }
        } catch (final IOException e) {
            throw new HbaseAdminException("Failed to create table! - " + tableName, e);
        }
    }
}
