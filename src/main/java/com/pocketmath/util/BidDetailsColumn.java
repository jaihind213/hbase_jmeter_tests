package com.pocketmath.util;

import org.apache.hadoop.hbase.util.Bytes;

import static com.pocketmath.util.BidDetailsColumnFamily.COUNTERS;
import static com.pocketmath.util.BidDetailsColumnFamily.DETAILS;
import static com.pocketmath.util.ValueGenerators.*;


/**
 * @author Pedro Gandola <pedro.gandola@pocketmath.com>.
 */
public enum BidDetailsColumn {
    BID_RESPONSE_ID       (true, new HashGenerator(), DETAILS, "bid_response_id"),
    IO_ID                 (false, new LongGenerator(5000), DETAILS, "io_id"),
    PARENT_IO_ID          (false, new LongGenerator(5000), DETAILS, "parent_io_id"),
    BID_TIME_MILLIS       (false, new TimestampGenerator(), DETAILS, "bid_time_millis"),
    WIN_TIME_MILLIS       (false, new TimestampGenerator(), DETAILS, "win_time_millis"),
    VIEW_TIME_MILLIS      (false, new TimestampGenerator(), DETAILS, "view_time_millis"),
    CLICK_TIME_MILLIS     (false, new TimestampGenerator(), DETAILS, "click_time_millis"),
    CONVERSION_TIME_MILLIS(false, new TimestampGenerator(), DETAILS, "conversion_time_millis"),
    DEVICE_OS_ID          (false, new LongGenerator(10), DETAILS, "device_os_id"),
    PLATFORM_ID           (false, new LongGenerator(5), DETAILS, "platform_id"),
    PUBLISHER_NAME        (false, new StringGenerator("publisher"), DETAILS, "publisher_name"),
    EXCHANGE_ID           (false, new LongGenerator(100),DETAILS, "exchange_id"),
    LOC_ID                (false, new LongGenerator(10000000),DETAILS, "loc_id"),
    BID_AMOUNT            (false, new LongGenerator(10000000),DETAILS, "bid_amount"),
    AMOUNT_PAID           (false, new LongGenerator(10000000),DETAILS, "amount_paid"),
    EXPIRES_AT            (false, new TimestampGenerator(),DETAILS, "expires_at"),
    HOUR_EPOCH            (false, new LongGenerator(100000),DETAILS, "hour_epoch"),
    DEVICE_MAKE           (false, new StringGenerator("device_make"),DETAILS, "device_make"),
    DEVICE_MODEL          (false, new StringGenerator("model"), DETAILS, "device_model"),
    DEVICE_MAC_SHA1       (false, new StringGenerator("sha1_of_the_device"), DETAILS, "device_mac_sha1"),
    DEVICE_IDENTIFIER     (false, new StringGenerator("device_identifier"), DETAILS, "device_identifier"),
    GEOLITE_ISP_COUNTRY   (false, new StringGenerator("country"), DETAILS, "geolite_isp_country"),
    GEOLITE_ISP_NAME      (false, new StringGenerator("isp"), DETAILS, "geolite_isp_name"),
    USER_IP               (false, new StringGenerator("ipaddress"), DETAILS, "user_ip"),
    USER_AGENT            (false, new StringGenerator("useragent"), DETAILS, "user_agent"),
    DNT                   (false, new StringGenerator("dnt"), DETAILS, "dnt"),
    CATEGORY              (false, new StringGenerator("IAB"),DETAILS, "category"),
    IAB_CATEGORY          (false, new StringGenerator("IAB"),DETAILS, "iab_category"),
    CACHEBUSTER           (false, new StringGenerator("cachebuster"),DETAILS, "cachebuster"),
    NETWORK_COOKIE_ID     (false, new StringGenerator("cookie_id"),DETAILS, "network_cookie_id"),
    POCKETMATH_COOKIE_ID  (false, new StringGenerator("pocketmath_cookie_id"),DETAILS, "pocketmath_cookie_id"),
    GPS_COORD_LAT         (false, new LongGenerator(1000000),DETAILS, "gps_coord_lat"),
    GPS_COORD_LON         (false, new LongGenerator(1000000),DETAILS, "gps_coord_lon"),
    IS_MOBILE             (false, new BooleanGenerator(),DETAILS, "is_mobile"),
    RAW_DEVICE_OS         (false, new StringGenerator("raw_os"),DETAILS, "raw_device_os"),
    CONNECTION_TYPE       (false, new StringGenerator("connection_type"),DETAILS, "connection_type"),
    CARRIER_NAME          (false, new StringGenerator("carrier_name"),DETAILS, "carrier_name"),
    APP_NAME              (false, new StringGenerator("app_name"),DETAILS, "app_name"),
    SITE_NAME             (false, new StringGenerator("site_name"),DETAILS, "site_name"),
    PAGE_URL              (false, new StringGenerator("page_url"),DETAILS, "page_url"),
    DEVICE_ID_IFA         (false, new UDIDGenerator(),DETAILS, "device_id_ifa"),
    DEVICE_ID_AID         (false, new UDIDGenerator(),DETAILS, "device_id_aid"),
    DEVICE_ID_DPID        (false, new UDIDGenerator(),DETAILS, "device_id_dpid"),
    DEVICE_ID_DID         (false, new UDIDGenerator(),DETAILS, "device_id_did"),
    RAW_BLOCKED_ADVERTISER(false, new UDIDGenerator(),DETAILS, "raw_blocked_advertiser"),
    RAW_BLOCKED_CATEGORY  (false, new UDIDGenerator(),DETAILS, "raw_blocked_category"),
    BLOCKED_CATEGORY      (false, new StringGenerator("IAB"),DETAILS, "blocked_category"),
    DEVICE_ID_UDID        (false, new UDIDGenerator(),DETAILS, "device_id_udid"),
    WURFL_DEVICE_ID       (false, new UDIDGenerator(),DETAILS, "wurfl_device_id"),
    WURFL_BRAND_NAME      (false, new StringGenerator("wurfl_brand"),DETAILS, "wurfl_brand_name"),
    WURFL_MODEL_NAME      (false, new StringGenerator("wurfl_model"),DETAILS, "wurfl_model_name"),
    IS_INTERSTITIAL       (false, new BooleanGenerator(),DETAILS, "is_interstitial"),
    IS_SMARTPHONE         (false, new BooleanGenerator(),DETAILS, "is_smartphone"),
    IS_TABLET             (false, new BooleanGenerator(),DETAILS, "is_tablet"),
    API_FRAMEWORKS        (false, new StringGenerator("api_frameworks"),DETAILS, "api_frameworks"),
    SKYHOOK_FLAG          (false, new IntGenerator(2),DETAILS, "skyhook_flag"),
    MISC_10               (false, new StringGenerator("misc10"),DETAILS, "misc_10"),
    CLICKS                (false, new LongGenerator(1), COUNTERS, "clicks"),
    CONVERSIONS           (false, new LongGenerator(1), COUNTERS, "conversions"),
    WINS                  (false, new LongGenerator(1), COUNTERS, "wins");

    private final boolean key;
    private final String alias;
    private final byte[] aliasBytes;
    private final ValueGenerator generator;
    private final BidDetailsColumnFamily family;

    private BidDetailsColumn(final boolean key, final ValueGenerator generator, final BidDetailsColumnFamily family, final String alias) {
        this.alias = alias;
        this.aliasBytes = Bytes.toBytes(alias);
        this.generator = generator;
        this.key = key;
        this.family = family;
    }

    public BidDetailsColumnFamily getFamily() {
        return family;
    }

    public String getAlias() {
        return alias;
    }

    public byte[] getAliasBytes() {
        return aliasBytes;
    }

    public ValueGenerator getGenerator() {
        return generator;
    }

    public boolean isKey() {
        return key;
    }
}
