package com.pocketmath.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by vishnuhr on 18/3/15.
 * Assumption: for the uuid to be truly, unique that the test runs from one jvm. todo: add ip address
 */
public class UUIDGenerator {

    private static final String DEFAULT_DELIMITER = "__";
    private static final String DEFAULT_HASH_ALGO = "SHA-256";
    private static AtomicLong uniqueLongId = new AtomicLong(System.currentTimeMillis());

    public static String generate(String delimiter) {
        if (delimiter == null || delimiter.isEmpty()) {
            delimiter = DEFAULT_DELIMITER;
        }
        long id = uniqueLongId.incrementAndGet();
        return String.valueOf(id) + delimiter + String.valueOf(Thread.currentThread().getId()) + delimiter + String.valueOf(System.currentTimeMillis());
        //assume tests run from one jvm.todo: add ip address
    }

    public static String generateUniqueHash(String delimiter) {
        String uuid = generate(delimiter);
        String uuidHash = null;
        try {
            MessageDigest md = MessageDigest.getInstance(DEFAULT_HASH_ALGO);
            md.update(uuid.getBytes());
            uuidHash = new String(md.digest().toString());
        } catch (NoSuchAlgorithmException willNeverOccur) {
        }

        return uuidHash;
    }

//    public static void main(String[] args) {
//        System.out.println(generateUniqueHash("**"));
//        System.out.println(generate(""));
//        System.out.println(generate("**"));
//    }
}
