package com.pocketmath.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by vishnuhr on 18/3/15.
 */
public class StringUtil {

    private static Map<Integer,String> cache = new HashMap<Integer, String>();//key is len, value is string having length=len.

    private static final String characterBank = "abcdefghijklmopqrstuvxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_#@%^&*()!+=%";
    private static Random rnd = new Random();

    public static String generateString(int length){
        String str = cache.get(length);
        if(null==str || str.isEmpty()){
            str= generate(length);
            cache.put(length,str);
        }
        return str;
    }


    private static String generate( int len )
    {
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append(characterBank.charAt(rnd.nextInt(characterBank.length())));
        return sb.toString();
    }


}
