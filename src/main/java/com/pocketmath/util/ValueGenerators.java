package com.pocketmath.util;

import org.apache.hadoop.hbase.util.Bytes;

import java.util.Random;
import java.util.UUID;

/**
 * @author Pedro Gandola <pedro.gandola@pocketmath.com>.
 */
public final class ValueGenerators {

    private ValueGenerators() {
    }

    public interface ValueGenerator<T> {
        public byte[] generateBytes();

        public T generateValue();
    }

    public static class LongGenerator implements ValueGenerator<Long> {
        private final int limit;
        private final Random rand = new Random();

        public LongGenerator(final int limit) {
            this.limit = limit;
        }

        @Override
        public byte[] generateBytes() {
            return Bytes.toBytes(generateValue());
        }

        @Override
        public Long generateValue() {
            return new Long(rand.nextInt(limit));
        }
    }

    public static class IntGenerator implements ValueGenerator<Integer> {
        private final int limit;
        private final Random rand = new Random();

        public IntGenerator(final int limit) {
            this.limit = limit;
        }

        @Override
        public byte[] generateBytes() {
            return Bytes.toBytes(generateValue());
        }

        @Override
        public Integer generateValue() {
            return rand.nextInt(limit);
        }
    }

    public static class StringGenerator implements ValueGenerator<String> {
        private final String prefix;
        private final Random rand = new Random();

        public StringGenerator(final String prefix) {
            this.prefix = prefix;
        }

        @Override
        public byte[] generateBytes() {
            return Bytes.toBytes(generateValue());
        }

        @Override
        public String generateValue() {
            return prefix + '_' + StringUtil.generateString(10);
        }
    }

    public static class BooleanGenerator implements ValueGenerator<Boolean> {
        private final Random rand = new Random();

        @Override
        public byte[] generateBytes() {
            return Bytes.toBytes(generateValue());
        }

        @Override
        public Boolean generateValue() {
            return rand.nextBoolean();
        }
    }

    public static class HashGenerator extends StringGenerator {

        public HashGenerator() {
            super("");
        }

        @Override
        public String generateValue() {
            return UUIDGenerator.generateUniqueHash("-");
        }
    }

    public static class TimestampGenerator extends LongGenerator {

        public TimestampGenerator() {
            super(0);
        }

        @Override
        public Long generateValue() {
            return System.currentTimeMillis();
        }
    }

    public static class UDIDGenerator extends StringGenerator {
        public UDIDGenerator() {
            super("");
        }

        @Override
        public String generateValue() {
            return UUID.randomUUID().toString();
        }
    }
}
