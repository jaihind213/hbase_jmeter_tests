package com.pocketmath.jmeter.test;

import com.pocketmath.exception.HbaseAdminException;
import com.pocketmath.jmeter.Constants;
import com.pocketmath.util.BidDetailsColumnFamily;
import com.pocketmath.util.HBaseTableUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.HTablePool;
import org.apache.hadoop.hbase.io.hfile.Compression;
import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.AbstractJavaSamplerClient;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vishnuhr on 18/3/15.
 */
public abstract class BaseTest extends AbstractJavaSamplerClient {

    protected Configuration hbaseConfig;
    protected HTablePool tablePool;// For now this pool is not shared by all threads but it should.
    protected int batchSize = 1;//no. of records to put in 1 put. cannot be zero!
    protected String tableName;//no. of records to put in 1 put. cannot be zero!
    protected String compressionAlgo = Compression.Algorithm.NONE.getName();

    @Override
    public void setupTest(JavaSamplerContext context) {
        super.setupTest(context);
        this.hbaseConfig = getHBaseConfiguration(context);
        this.batchSize = Math.max(1, Integer.parseInt(context.getParameter(Constants.BULK_LOAD_BATCH_SIZE)));

        this.tableName = context.getParameter(Constants.HBASE_TABLE);
        this.compressionAlgo = context.getParameter(Constants.COMPRESSION_ALGO);
        final List<String> columnFamilyNames = new ArrayList<>();
        //todo: make table cf configurable
        for (final BidDetailsColumnFamily columnFamily : BidDetailsColumnFamily.values()) {
            columnFamilyNames.add(columnFamily.name());
        }

        // Creates or checks the Hbase table.
        try {
            HBaseTableUtil.createTable(tableName, columnFamilyNames, this.hbaseConfig, compressionAlgo);
        } catch (final HbaseAdminException ex) {
            throw new IllegalStateException("Unable to create/check the table: " + tableName, ex);
        }

        // creates the pool of tables.
        this.tablePool = new HTablePool(this.hbaseConfig, 1);
    }

    @Override
    public abstract SampleResult runTest(JavaSamplerContext javaSamplerContext) ;

    private Configuration getHBaseConfiguration(JavaSamplerContext testContext) {
        String master = testContext.getParameter(Constants.HBASE_MASTER);
        String zkPort = testContext.getParameter(Constants.HBASE_ZOOKEEPER_CLIENTPORT);
        String zkQuorum = testContext.getParameter(Constants.HBASE_ZOOKEEPER_QUORUM);
        Configuration configuration = HBaseConfiguration.create();
        configuration.set(Constants.HBASE_MASTER, master);
        configuration.set(Constants.HBASE_ZOOKEEPER_CLIENTPORT, zkPort);
        configuration.set(Constants.HBASE_ZOOKEEPER_QUORUM, zkQuorum);
        return configuration;
    }

    protected void failTest(Exception cause, final SampleResult result) {
        result.setSuccessful(false);
        result.setResponseMessage("Exception: " + cause);
        // get stack trace as a String to return as document data
        java.io.StringWriter stringWriter = new java.io.StringWriter();
        cause.printStackTrace(new java.io.PrintWriter(stringWriter));
        result.setResponseData(stringWriter.toString());
        result.setDataType(org.apache.jmeter.samplers.SampleResult.TEXT);
        result.setResponseCode("500");
    }

    protected void passTest(String message, final SampleResult result) {
        result.setSuccessful(true);
        result.setResponseMessage(message);
        result.setResponseCodeOK(); // 200 code
    }

    @Override
    public void teardownTest(JavaSamplerContext context) {
        super.teardownTest(context);
        if (tablePool != null) {
            try {
                tablePool.close();
            } catch (IOException ignore) {
            }
        }
    }

    @Override
    public Arguments getDefaultParameters() {
        Arguments args = new Arguments();

        //for configuring properties in jmeter commandline we use the format
        //${__P(<variable name>,<default value>)}
        //https://mkbansal.wordpress.com/2012/08/01/jmeter-command-line-script-execution-with-arguments/
        args.addArgument(Constants.HBASE_MASTER, "${__P(hbase_master,localhost:600000)}");
        args.addArgument(Constants.HBASE_ZOOKEEPER_CLIENTPORT, "${__P(hbase_zk_port,2181)}");
        args.addArgument(Constants.HBASE_ZOOKEEPER_QUORUM, "${__P(hbase_zk_quorum,localhost)}");
        args.addArgument(Constants.ROW_KEY_LENGTH, "100");
        args.addArgument(Constants.HBASE_TABLE, "${__P(hbase_table,test)}");
        args.addArgument(Constants.NO_COLUMN_FAMILIES, "${__P(no_column_families,2)}");
        args.addArgument(Constants.NO_OF_COLUMN_PER_CF, "${__P(no_columns_per_cf,1)}");
        args.addArgument(Constants.DATA_LENGTH_IN_COLUMN, "${__P(data_len_column,1)}");
        args.addArgument(Constants.BULK_LOAD_BATCH_SIZE, "${__P(batch_size,1)}");
        return args;
    }

}
