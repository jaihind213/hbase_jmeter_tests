package com.pocketmath.jmeter.test.phoenix;

import com.pocketmath.util.BidDetailsColumn;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.pocketmath.util.BidDetailsColumn.BID_RESPONSE_ID;


/**
 * Created by vishnuhr on 18/3/15.
 */
public class PhoenixPutTest extends PhoenixBaseTest {

    private String upsertQuery;
    public PhoenixPutTest() {

    }

    @Override
    public void setupTest(JavaSamplerContext context) {
        super.setupTest(context);
        this.upsertQuery = String.format("UPSERT into %s values (%s)", tableName, StringUtils.repeat("?", ",", BidDetailsColumn.values().length));
    }

    @Override
    protected void writeBatch(final Connection connection) throws SQLException {
        try (final PreparedStatement stmt = connection.prepareStatement(this.upsertQuery)) {
            for (int i = 0; i < batchSize; i++) {
                int index = 1;
                for (final BidDetailsColumn column : BidDetailsColumn.values()) {
                    //TODO: This might be slow we need to improve this
                    stmt.setObject(index++, column.getGenerator().generateValue());
                }
                stmt.executeUpdate();
            }
            connection.commit();
        } catch (final SQLException ex) {
            ex.printStackTrace();
            connection.rollback();
        }
    }
}

