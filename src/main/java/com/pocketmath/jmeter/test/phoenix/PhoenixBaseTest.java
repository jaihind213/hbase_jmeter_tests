package com.pocketmath.jmeter.test.phoenix;

import com.pocketmath.jmeter.Constants;
import com.pocketmath.util.BidDetailsColumnFamily;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.AbstractJavaSamplerClient;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class PhoenixBaseTest extends AbstractJavaSamplerClient {

    protected int batchSize = 1;
    protected String tableName;
    protected String host;
    protected boolean compression;
    protected int buckets;
    protected HikariDataSource dataSource;

    private final static String CREATE_TABLE_QUERY_TEMPLATE = "CREATE TABLE IF NOT EXISTS %s (bid_response_id varchar not null primary key, io_id bigint, parent_io_id bigint, bid_time_millis bigint, win_time_millis bigint, view_time_millis bigint, click_time_millis bigint, conversion_time_millis bigint, device_os_id bigint, platform_id bigint, publisher_name varchar, exchange_id bigint, loc_id bigint, bid_amount bigint, amount_paid bigint, expires_at bigint, hour_epoch bigint, device_make varchar, device_model varchar, device_mac_sha1 varchar, device_identifier varchar, geolite_isp_country varchar, geolite_isp_name varchar, user_ip varchar, user_agent varchar, dnt varchar, category varchar, iab_category varchar, cachebuster varchar, network_cookie_id varchar, pocketmath_cookie_id varchar, gps_coord_lat bigint, gps_coord_lon bigint, is_mobile boolean, raw_device_os varchar, connection_type varchar, carrier_name varchar, app_name varchar, site_name varchar, page_url varchar, device_id_ifa varchar, device_id_aid varchar, device_id_dpid varchar, device_id_did varchar, raw_blocked_advertiser varchar, raw_blocked_category varchar, blocked_category varchar, device_id_udid varchar, wurfl_device_id varchar, wurfl_brand_name varchar, wurfl_model_name varchar, is_interstitial boolean, is_smartphone boolean, is_tablet boolean, api_frameworks varchar, skyhook_flag integer, misc_10 varchar, clicks bigint, conversions bigint, wins bigint) %s %s";
    private final static String SALT_BUCKETS_SUFFIX = ", SALT_BUCKETS = %d";
    private final static String COMPRESSION_SUFFIX = "COMPRESSION='SNAPPY'";
    private final static String COMPRESSION_SUFFIX_NONE = "COMPRESSION='NONE'";

    @Override
    public void setupTest(JavaSamplerContext context) {
        super.setupTest(context);

        // Print all properties
        /*Iterator<String> itr = context.getParameterNamesIterator();
        itr.forEachRemaining(str -> {
            System.out.println("Property: " + str + " Value: " + context.getParameter(str));
        });*/

        this.compression = Boolean.valueOf(context.getParameter(Constants.PHOENIX_COMPRESSION));
        this.buckets = Integer.valueOf(context.getParameter(Constants.PHOENIX_SALT_BUCKETS));
        this.batchSize = Math.max(1, Integer.parseInt(context.getParameter(Constants.BULK_LOAD_BATCH_SIZE)));
        this.tableName = context.getParameter(Constants.PHOENIX_TABLE);
        this.host = context.getParameter(Constants.PHOENIX_HOST);

        final List<String> columnFamilyNames = new ArrayList<>();
        for (final BidDetailsColumnFamily columnFamily : BidDetailsColumnFamily.values()) {
            columnFamilyNames.add(columnFamily.name());
        }

        this.dataSource = new HikariDataSource();
        this.dataSource.setJdbcUrl("jdbc:phoenix:" + this.host);
        this.dataSource.setConnectionTestQuery("");
        this.dataSource.setDriverClassName("org.apache.phoenix.jdbc.PhoenixDriver");
        this.dataSource.setAutoCommit(false);
        this.dataSource.setMaximumPoolSize(50);

        //Create Table
        try (final Connection connection = dataSource.getConnection()) {
            try (final Statement statement = connection.createStatement()) {
                final String query = String.format(CREATE_TABLE_QUERY_TEMPLATE, tableName,
                        (compression) ? COMPRESSION_SUFFIX : COMPRESSION_SUFFIX_NONE,
                        (buckets > 1) ? String.format(SALT_BUCKETS_SUFFIX, buckets) : "");
           //     System.out.println(query);
                statement.executeUpdate(query);
                connection.commit();
            } catch (final SQLException ex) {
                try {
                    connection.rollback();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                throw new RuntimeException("Unable to create the table:" + tableName, ex);
            }
        } catch (final SQLException ex) {
            throw new RuntimeException("Unable to create the table:" + tableName, ex);
        }
    }

    @Override
    public SampleResult runTest(JavaSamplerContext javaSamplerContext) {
        final SampleResult result = new SampleResult();
        result.sampleStart();
        try (final Connection connection = dataSource.getConnection()) {
            writeBatch(connection);
            result.sampleEnd();
            passTest("Batch was inserted with success!", result);
        } catch (final SQLException ex) {
            result.sampleEnd();
            failTest(ex, result);
            ex.printStackTrace();
        }
        return result;
    }

    protected abstract void writeBatch(final Connection connection) throws SQLException;

    protected void failTest(Exception cause, final SampleResult result) {
        result.setSuccessful(false);
        result.setResponseMessage("Exception: " + cause);
        // get stack trace as a String to return as document data
        java.io.StringWriter stringWriter = new java.io.StringWriter();
        cause.printStackTrace(new java.io.PrintWriter(stringWriter));
        result.setResponseData(stringWriter.toString());
        result.setDataType(SampleResult.TEXT);
        result.setResponseCode("500");
    }

    protected void passTest(String message, final SampleResult result) {
        result.setSuccessful(true);
        result.setResponseMessage(message);
        result.setResponseCodeOK(); // 200 code
    }

    @Override
    public void teardownTest(JavaSamplerContext context) {
        super.teardownTest(context);
        if (dataSource != null) {
            dataSource.shutdown();
        }
    }

    @Override
    public Arguments getDefaultParameters() {
        final Arguments args = new Arguments();
        args.addArgument(Constants.PHOENIX_HOST, "${__P(phoenix_host,localhost)}");
        args.addArgument(Constants.PHOENIX_TABLE, "${__P(phoenix_table,localhost)}");
        args.addArgument(Constants.BULK_LOAD_BATCH_SIZE, "${__P(batch_size,1)}");
        return args;
    }
}
