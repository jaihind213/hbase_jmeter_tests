package com.pocketmath.jmeter.test.hbase;

import com.pocketmath.jmeter.test.hbase.WriteBaseTest;
import com.pocketmath.util.BidDetailsColumn;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.pocketmath.util.BidDetailsColumn.BID_RESPONSE_ID;


/**
 * Created by vishnuhr on 18/3/15.
 */
public class HBasePutTest extends WriteBaseTest {

    @Override
    protected void writeBatch(final HTableInterface table) throws IOException, InterruptedException {
        final List<Put> putRequests = new ArrayList<>();
        for (int i = 0; i < batchSize; i++) {
            final Put put = new Put(BID_RESPONSE_ID.getGenerator().generateBytes());
            for (final BidDetailsColumn column : BidDetailsColumn.values()) {
                if (!column.isKey()) {
                    put.add(column.getFamily().getNameBytes(),
                            column.getAliasBytes(),
                            column.getGenerator().generateBytes());

                }
            }
            putRequests.add(put);
        }
        table.put(putRequests);
    }
}

