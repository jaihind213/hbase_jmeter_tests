package com.pocketmath.jmeter.test.hbase;

import com.pocketmath.util.BidDetailsColumn;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Increment;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Row;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import static com.pocketmath.util.BidDetailsColumn.BID_RESPONSE_ID;
import static com.pocketmath.util.BidDetailsColumn.WINS;

/**
 * This class mixes writes with 20 % of increments and eventually resuses some of the already generated bidIds as Id
 * for increments.
 */
public class HBasePutAndIncTest extends WriteBaseTest {

    private Queue<byte[]> bidIdQueue = new LinkedList<>();
    private static final int QUEUE_MAX_SIZE = 1000;

    @Override
    protected void writeBatch(final HTableInterface table) throws IOException, InterruptedException {
        final List<Row> putRequests = new ArrayList<>();
        for (int i = 0; i < batchSize; i++) {
            final Row row;
            // 10% of the writes are increments
            if (Math.random() <= 0.20d) {
                // gets the id from one of the existing ones or a new one.
                final byte[] bidId = (!bidIdQueue.isEmpty()) ? bidIdQueue.poll() : BID_RESPONSE_ID.getGenerator().generateBytes();
                final BidDetailsColumn counter = WINS;
                row = new Increment(bidId).addColumn(counter.getFamily().getNameBytes(), counter.getAliasBytes(), 1L);
            } else {
                byte[] bidId = BID_RESPONSE_ID.getGenerator().generateBytes();
                final Put put = new Put(bidId);
                for (final BidDetailsColumn column : BidDetailsColumn.values()) {
                    if (!column.isKey()) {
                        put.add(column.getFamily().getNameBytes(),
                                column.getAliasBytes(),
                                column.getGenerator().generateBytes());
                    }
                }

                // Cache a bidid to reuse for future Increments.
                if (Math.random() <= 0.70d) {
                    if (bidIdQueue.size() < QUEUE_MAX_SIZE) {
                        bidIdQueue.add(bidId);
                    }
                }
                row = put;
            }
            putRequests.add(row);
        }
        table.batch(putRequests);
    }
}
