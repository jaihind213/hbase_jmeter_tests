package com.pocketmath.jmeter.test.hbase;

import com.pocketmath.jmeter.test.BaseTest;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;

import java.io.IOException;

/**
 * Created by vishnuhr on 23/3/15.
 */
public abstract class WriteBaseTest extends BaseTest {

    @Override
    public SampleResult runTest(JavaSamplerContext javaSamplerContext) {
        final SampleResult result = new SampleResult();
        result.sampleStart();
        try (final HTableInterface table = tablePool.getTable(this.tableName)) {
            writeBatch(table);
            result.sampleEnd();
            passTest("Batch was inserted with success!", result);
        } catch (final IOException | InterruptedException ex) {
            result.sampleEnd();
            failTest(ex, result);
            ex.printStackTrace();
        }
        return result;
    }

    protected abstract void writeBatch(HTableInterface table) throws IOException, InterruptedException;
}
