package com.pocketmath.jmeter;

/**
 * Created by vishnuhr on 18/3/15.
 */
public interface Constants {

    public final static String HBASE_ZOOKEEPER_CLIENTPORT = "hbase.zookeeper.property.clientPort";
    public final static String HBASE_ZOOKEEPER_QUORUM = "hbase.zookeeper.quorum";
    public final static String HBASE_MASTER = "hbase.master";
    public final static String HBASE_TABLE = "hbase.table.name";
    public final static String COMPRESSION_ALGO = "compression_algo";

    //*****************************************************************************

    public final static String ROW_KEY_DELIMITER= "row.key.delimiter";
    public final static String ROW_KEY_LENGTH= "row.key.length";

    //*****************************************************************************

    public final static String NO_COLUMN_FAMILIES= "no_column_families";
    public final static String NO_OF_COLUMN_PER_CF= "no_of_column_per_cf";

    /** length of data in each column for a column family*/
    public final static String DATA_LENGTH_IN_COLUMN = "data_length_in_column";

    public final static String columnFamilyNamePrefix = "cf_";
    public final static String columnNamePrefix = "c_";
    //*****************************************************************************

    public final static String BULK_LOAD_BATCH_SIZE = "bulk_load_batch_size";

    public final static String PHOENIX_HOST = "phoenix_host";
    public final static String PHOENIX_TABLE = "phoenix_table";

    public final static String PHOENIX_COMPRESSION = "phoenix_compression";
    public final static String PHOENIX_SALT_BUCKETS = "phoenix_salt_buckets";
}
