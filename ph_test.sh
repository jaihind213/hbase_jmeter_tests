#!/bin/sh

#echo "usage ./test.sh <table_name> <no_threads> <batch_size> <duration> <path_to_jmx_plan>"
if [ "$1" == "-help" ];then
	echo "usage ./ph_test.sh <table_name> <no_threads> <batch_size> <duration> <path_to_jmx_plan> <buckets>"
	exit -1
fi
table=$1
no_threads=$2
batch_size=$3
duration=$4
path_to_jmx_plan=$5
buckets=$6

if [[ ( "$1" == "" ) || ( "$2" == "" ) || ( "$3" == "" ) || ( "$4" == "" ) || ( "$5" == "" ) || ( "$6" == "" ) ]];then
        echo "usage ./ph_test.sh <table_name> <no_threads> <batch_size> <duration> <path_to_jmx_plan> <buckets>"
        exit -1
fi

export JAVA_HOME=/home/vishnuhr/utils/java/jdk1.8.0_40
export PATH=$JAVA_HOME/bin:$PATH
export JMETER_HOME=~/utils/apache-jmeter-2.13
export PATH=$JMETER_HOME/bin:$PATH

#jmeter -n -t $path_to_jmx_plan  -Jhbase_master=pulsar-emr.posumeads.com:600000 -Jhbase_zk_port=2181 -Jhbase_zk_quorum=pulsar-emr.posumeads.com -Jhbase_table=$table -Jduration=$duration -Jbatch_size=$batch_size -l test.log -j run.log -Jno_threads=$no_threads
#phoenix_jmeter_plan.jmx
echo $buckets
jmeter -n -t $path_to_jmx_plan -Jno_threads=$no_threads -Jduration=$duration -Jphoenix_host=pulsar-emr.posumeads.com:2181 -Jphoenix_table=$table -Jbatch_size=$batch_size -Jphoenix_compression=false -Jphoenix_salt_buckets=$buckets  -l test.log -j run.log
